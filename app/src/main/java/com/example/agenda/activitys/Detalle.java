package com.example.agenda.activitys;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.agenda.activitys.ui.alta.AltaFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.agenda.R;

public class Detalle extends AppCompatActivity {
    TextView nombre, cumpleaño, telefono, nota;
    Button edit;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //Window window = getWindow();
        //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));


        nombre = (TextView) findViewById(R.id.nombre);
        cumpleaño = (TextView) findViewById(R.id.cumpleaños);
        telefono = (TextView) findViewById(R.id.telefono);
        nota = (TextView) findViewById(R.id.note);

      /*  edit = (Button) findViewById(R.id.editar);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AltaFragment altaFragment = new AltaFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.nav_host_fragment, altaFragment);
                fragmentTransaction.commit();

            }
        });*/
        // String i = getIntent().getStringExtra("id");
        String name = getIntent().getStringExtra("name");
        String phone = getIntent().getStringExtra("phone");
        String birthday = getIntent().getStringExtra("birthday");
        String note = getIntent().getStringExtra("note");
        nombre.setText(name);
        cumpleaño.setText(birthday);
        telefono.setText(phone);
        nota.setText(note);


    }

}

package com.example.agenda.activitys.ui.lista;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agenda.R;
import com.example.agenda.SQLhelper.SQLiteHelper;
import com.example.agenda.SQLhelper.Tablas;
import com.example.agenda.activitys.Detalle;
import com.example.agenda.model.AgendaModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<AgendaModel> dataModelArrayLis;

    public MyAdapter(Context ctx, ArrayList<AgendaModel> dataModelArrayLis) {

        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayLis = dataModelArrayLis;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.recycler_view_list, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;

    }

    @Override
    public void onBindViewHolder(MyAdapter.MyViewHolder holder, int position) {

        holder.nombre.setText(dataModelArrayLis.get(position).getNombre());
    final   String phone="Phone: ".concat(dataModelArrayLis.get(position).getTelefono());
        holder.telefono.setText(phone);
        String birthday="Birthday: ".concat(dataModelArrayLis.get(position).getCumpleaños());
        holder.cumpleaño.setText(birthday);
        holder.nota.setText(dataModelArrayLis.get(position).getNota());

        holder.id = dataModelArrayLis.get(position).getId();
        holder.nombreA = dataModelArrayLis.get(position).getNombre();
        holder.telefonoA = dataModelArrayLis.get(position).getTelefono();
        holder.cumpleañoA = dataModelArrayLis.get(position).getCumpleaños();
        holder.notaA = dataModelArrayLis.get(position).getNota();


    }

    @Override
    public int getItemCount() {
        return dataModelArrayLis.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView nombre, telefono, cumpleaño, nota;
        int id;
        String nombreA,telefonoA,cumpleañoA,notaA;

        public MyViewHolder(final View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.nombre);
            telefono = (TextView) itemView.findViewById(R.id.telefono);
            cumpleaño = (TextView) itemView.findViewById(R.id.cumpleaños);
            nota = (TextView) itemView.findViewById(R.id.nota);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    SQLiteHelper conect;
                    conect = new SQLiteHelper(v.getContext(), "bd_agenda", null, 1);

                    final SQLiteDatabase db = conect.getReadableDatabase();
                    new AlertDialog.Builder(v.getContext())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Remover Agenda")
                            .setMessage("Estas seguro,se eliminara permanentemente?")
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    db.delete(Tablas.NOMBRE_TABLA, "id = ?",
                                            new String[]{String.valueOf(id)});
                                    db.close();
                                    return;
                                }

                            }).setNegativeButton("No", null).show();

                    return false;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), Detalle.class);
                    intent.putExtra("id", id);
                     intent.putExtra("name", nombreA);
                     intent.putExtra("phone", telefonoA);
                     intent.putExtra("birthday", cumpleañoA);
                     intent.putExtra("note", notaA);
                    v.getContext().startActivity(intent);

                }
            });
        }


    }
}
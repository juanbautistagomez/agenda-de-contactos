package com.example.agenda.activitys.ui.alta;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.agenda.R;
import com.example.agenda.SQLhelper.SQLiteHelper;
import com.example.agenda.SQLhelper.Tablas;
import com.example.agenda.activitys.ui.lista.AgendaListFragment;
import com.google.android.material.textfield.TextInputEditText;

import java.net.ContentHandler;

public class AltaFragment extends Fragment {
    private TextInputEditText agenda_nombre, agenda_telefono, agenda_cumpleaño, agenda_nota;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // getActivity().setTitle("Nueva agenda");
        View root = inflater.inflate(R.layout.fragment_alta, container, false);
        agenda_nombre = root.findViewById(R.id.agenda_nombre);
        agenda_telefono = root.findViewById(R.id.agenda_telefono);
        agenda_cumpleaño = root.findViewById(R.id.agenda_cumpleaño);
        agenda_nota = root.findViewById(R.id.agenda_nota);
        final ImageView agenda_guardar = root.findViewById(R.id.agenda_guardar);
        ImageView cancelar = root.findViewById(R.id.cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
        agenda_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrarAgenda();
            }
        });
        agenda_cumpleaño.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        return root;
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = day + "/" + (month + 1) + "/" + year;
                agenda_cumpleaño.setText(selectedDate);
            }
        });

        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    private void RegistrarAgenda() {
        SQLiteHelper conect = new SQLiteHelper(getContext(), "bd_agenda", null, 1);
        SQLiteDatabase db = conect.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (agenda_nombre.getText().toString().isEmpty()) {
            agenda_nombre.setError("Es necesario");
            return;

        } else if (agenda_telefono.getText().toString().isEmpty()) {
            agenda_telefono.setError("Es necesario");
            return;
        } else if (agenda_cumpleaño.getText().toString().isEmpty()) {
            agenda_cumpleaño.setError("Es necesario");
            return;
        } else {
            contentValues.put(Tablas.CAMPO_NOMBRE, agenda_nombre.getText().toString().trim());
            contentValues.put(Tablas.CAMPO_TELEFONO, agenda_telefono.getText().toString().trim());
            contentValues.put(Tablas.CAMPO_CUMPLEAÑOS, agenda_cumpleaño.getText().toString().trim());
            contentValues.put(Tablas.CAMPO_NOTA, agenda_nota.getText().toString().trim());
            Long reault = db.insert(Tablas.NOMBRE_TABLA, Tablas.CAMPO_ID, contentValues);


            if (reault != null) {
                agenda_nombre.setText("");
                agenda_cumpleaño.setText("");
                agenda_telefono.setText("");
                agenda_nota.setText("");
                new AlertDialog.Builder(getContext())
                        .setTitle("Agregar mas agenda?")
                        .setMessage("No, ir al inicio")
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                regresar();
                                return;
                            }

                        }).setNegativeButton("Si", null).show();


            }

          //  Toast.makeText(getContext(), "Registrado exitosamente" + reault, Toast.LENGTH_SHORT).show();
        }
    }

    private void regresar() {
        AgendaListFragment fragment = new AgendaListFragment();
        FragmentManager fragmentManager = getFragmentManager();
        // Reemplazar intento con Bundle y ponerlo en la transacción
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.nav_host_fragment, fragment);
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
        fragmentTransaction.commit();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

}
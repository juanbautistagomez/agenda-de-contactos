package com.example.agenda.activitys.ui.lista;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.agenda.R;
import com.example.agenda.SQLhelper.SQLiteHelper;
import com.example.agenda.SQLhelper.Tablas;
import com.example.agenda.activitys.ui.alta.AltaFragment;
import com.example.agenda.model.AgendaModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AgendaListFragment extends Fragment {

SQLiteHelper conect;

    FloatingActionButton fab;
    private MyAdapter mAdapter;
    private RecyclerView recycler;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Agendas");
        View root = inflater.inflate(R.layout.fragment_lista, container, false);
        fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AltaFragment fragment = new AltaFragment ();
                FragmentManager fragmentManager = getFragmentManager ();
                // Reemplazar intento con Bundle y ponerlo en la transacción
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction ();
                fragmentTransaction.add (R.id.nav_host_fragment, fragment);
                fragmentTransaction.replace (R.id.nav_host_fragment, fragment);
                fragmentTransaction.commit ();
                ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

            }
        });
        conect = new SQLiteHelper(getContext(),"bd_agenda",null,1);
        recycler = root.findViewById(R.id.agendaRecycler);

        SQLiteDatabase db = conect.getReadableDatabase();
        AgendaModel regist = null ;
        ArrayList<AgendaModel> agendaModels = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Tablas.NOMBRE_TABLA,null);
        while (cursor.moveToNext()){
            regist = new AgendaModel();
            regist.setId(cursor.getInt(0));
            regist.setNombre(cursor.getString(1));
            regist.setTelefono(cursor.getString(2));
            regist.setCumpleaños(cursor.getString(3));
            regist.setNota(cursor.getString(4));
        //    agendaModelArrayList.add(regist);
          //  lista.setAdapter(regist);

          agendaModels.add(regist);
               Log.i("TAG",""+regist);


        }
        mAdapter = new MyAdapter(getActivity(),agendaModels);
        recycler.setAdapter(mAdapter);

        recycler.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        return root;
    }


}
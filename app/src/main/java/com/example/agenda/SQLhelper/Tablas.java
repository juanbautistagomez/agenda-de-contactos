package com.example.agenda.SQLhelper;

public class Tablas {
    public static final String NOMBRE_TABLA = "agenda";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "agenda_nombre";
    public static final String CAMPO_TELEFONO = "agenda_telefono";
    public static final String CAMPO_CUMPLEAÑOS = "agenda_cumpleaños";
    public static final String CAMPO_NOTA = "agenda_nota";

    public static final String CREATE_TABLE_AGENDA = "CREATE TABLE " + NOMBRE_TABLA + " (" + CAMPO_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CAMPO_NOMBRE
            + " TEXT, " + CAMPO_TELEFONO + " TEXT, " + CAMPO_CUMPLEAÑOS + " TEXT, "
            + CAMPO_NOTA + " TEXT)";

}
